## Artificer

### Ideas:
* subclasses:
  * alchemy
    * Idea: alchemical reactions
    * Role: improve spellcasting with potions and non-magical spell-ish things, also more/better cantrips
  * runesmith
    * Idea: draw runes on stuff to add magical effects
    * Role: be better in combat through effects that trigger automatically

# Spellsmith
#### Ideas
* change damage of one lvl 1 spell or cantrip to force
* Sanctum: one place where you can teleport back to or smt, maybe have some interactions
* Grimoire: you spellbook is magical and casts spells on who reads it
* charge spells
* make scrolls
*

# Necromancer variant
* See souls, put them in black onyx items. Resurrect them into undeath or empower your spells with them.
* Steal hitpoints from undead

### Secrets
Secret spells are only available as a one-time choice to the *Lore keeper* subclass. Any other character wishing to learn these spells must gain the knowledge from a *Lore keeper* who knows the spells.

##### Artificial animation
*6th level Necromancy*  
**Casting time:** 1 action.  
**Range:** Touch  
**Components:** S, M (the feather of a phoenix worth 300gp)  
**Duration:** Concentration, up to 10 minutes

You touch a creature and infuse it with necromantic energies.
While the creature is within 60 feet of you, you can use your bonus action to undo any physical harm it has received. This includes damage from spells



##### Soul host
*3th level necromancy*  
**Casting time:** 1 reaction, taken whenever a creature within 30ft of you dies  
**Range:** 30ft.  
**Components:**  S, M (any item worth at least 10gp)  
**Duration:** Instantaneous  

As you see a creature within range die, you touch a non-magical item on your person and turn it into a suitable container for a willing soul. If the creature's soul is willing, it may inhabit the object, becoming bound to it. The soul is protected from any entity, such as a god or demon, that may attempt to claim it for as long as the object is intact. The object becomes magical for as long as the soul resides in it. You may use this spell multiple times to reside multiple souls into one item, the item must be worth at least 10gp for every sould that resides in it.

You may use an action to release and empower the souls you have collected. When you do, choose up to 5 souls in containers you are carrying, each soul deals 1d6 psychic, necrotic or cold damage (your choice) to up to 1 creature within 10 feet of you it considers an enemy.

You may also speak with collected souls while taking a short or long rest.

***At higher levels:*** If you cast this spell using a 8th level spell slot or higher, you may trap an unwilling soul instead. When you use your action to release an unwillingly trapped soul, you may choose who it considers an enemy.


##### Spellbender
*Abjuration*  
Change the target of a spell


##### Reshape body
*5th level Transmutation*  

You permanently change a willing target's body. Besides altering its outward appearance, you can also change its race and subrace, changing the creature's statistics accordingly. This procedure is excruciatingly painful. As a result of this, the target takes 3d12 psychic damage. You, the caster, also take 3d12 psychic damage, reduced by 1d12 for every time you previously cast this spell, as you listen to the screams of your subject. Damage inflicted by this spell cannot be resisted or prevented. If the damage inflicted by this spell drops you to 0 hitpoints, the spell fails and the target becomes an abominable in-between version of the two shapes. After a creature has been the target of this spell once, it must succeed on a wisdom saving throw or it cannot be a willing target for this spell again. It may retake the saving throw after completing a long rest.

The new body is not magical, it is a normal body of the chosen type.

### Templates

### Spell ideas

##### Blood bridge
*1st level necromancy*  
**Casting time:** 1 action  
**Range:** Touch   
**Components:** V, S  
**Duration:** Concentration, up to 10 minutes

You touch two willing targets, siphoning blood through you from one to the other. You can reduce one target's hitpoints by 2d4 and increase the other target's hitpoints by an equal amount every turn. Hitpoints lost this way cannot be regained until after having spent a long rest and drunk a sufficient amount of water. A target reduced to 0 hitpoints by this spell dies immediately.

If a target becomes unwilling during casting he is considered grappled by you (if you desire) and will break the spell if it escapes your grapple. Breaking your concentration still ends the spell as normal.


##### Death recall
*2nd level necromancy (ritual)*  
**Casting time:** 10 minutes  
**Range:** Touch  
**Components:** V, S  
**Duration:** instantaneous  

You touch a body part of a dead humanoid creature. You learn different things depending on what body part you chose.
An eye will tell you the last thing a creature has seen.
A heart will give you an idea of how much the creature has sinned in its life. This does not work on inherently evil creatures as they do not consider evil deeds to be sinful.
Skin measuring at least 2 square feet will tell you how much the creature was loved or hated.
A liver will tell you the cause of death.

Alternatively, if the body in complete, you may relive the last 10 minutes of this body's life, you will experience only what the creature experienced.

This spell cannot be cast on the same target more than once.


##### Haunt
*6th level necromancy*  
**Casting time:** 1 hour  
**Range:** Touch  
**Components:** V, S  
**Duration:** 100 years  

You awaken and empower the angry spirits trapped in the land, making them relive and reenact the tragic events that bound them to this place.

Any creature entering a circle with a 300 feet radius takes 2d4 psychic damage and 1d6 necrotic damage every minute it stays in the area, as it is harassed by disembodied spirits.

Any corpses or bones left in the area will rise as zombies or skeletons and walk the area aimlessly, attacking anyone who might approach.

This effect cannot be ended by you nor can it be dispelled by the dispel magic spell as it is no longer considered a magical effect after it has been cast. Instead, the spirits must be laid back to rest.

##### Locate soul
*6th level divination*  
**Casting time:** 1 minute  
**Range:** Infinite  
**Components:** V, S, M (a diamond worth at least 1000gp)  
**Duration:** Instantaneous  

You seek out a soul you are familiar. You must have a strong bond with it, such as you might have with a parent, sibling, long-time friend, lover or bitter rival. You immediately know the location of the soul across both space and planes, and whether it is free.

##### Shape unlife
*5th level necromancy*  
**Casting time:** 1 action  
**Range:** Touch  
**Components:** V, S, M (any common item created from animal remains)  
**Duration:** 10 hours.  

You touch a pile of bones or corpses. You reshape infuse the remains with unlife and reshape them to your will. You may create a structure, a siege engine or a creature with cr 2 or lower. If you create a structure, it can move any moving parts such as doors. If you create a siege engine, the DM has its statistics, it can take an action each turn.

Whatever you create is considered an undead creature. A building has ac 0, 10 damage reduction and 2d6 hitpoints for every 200 lbs. of corpses it was created from. Your creation obeys your spoken commands but is otherwise idle.


##### Spasm
*Necromancy cantrip*  
**Casting time:** 1 bonus action   
**Range:** 60 ft.   
**Components:** S, M (a frog's leg)  
**Duration:** Instantaneous

You cause a corpse to briefly and suddenly move, causing it to trip, claw and bite an adjacent creature. Target a corpse within range. One creature of your choice within 5 feet of the corpse must make a dexterity saving throw or be knocked prone. If you hold this spell and trigger it when a creature moves past the corpse, the creature makes the saving throw at disadvantage.

This spell deals 1d4 damage piercing damage to the tripped creature when you reach 5th level, increasing by 1d4 at 11th level (2d4) and 17th level (3d4).

##### Fluids
*Necromancy*  
Drain your own bodily fluids and drown people in them

##### Magic anti-missile
*Abjuration or evocation*  
Shoots projectiles out of the air

##### Delayed illusion
*Illusion*  
Delay the effect of an illusion spell to make it look as though it failed and give the opponent disadvantage on saving throws.

##### Unholy Corruption
*1st level necromancy*  
**Casting time:** 1 action  
**Range:** 60ft.  
**Components:** S, M (a piece of an undead creature)  
**Duration:** Instantaneous  

You point as a creature and wash necrotic energies over it. It must make a wisdom saving throw. On a success, it takes 1d4 necrotic damage. On a failure, it's type becomes *undead* for the next 1d4 rounds. A creature may choose to fail the saving throw, but if it is a divine caster, doing so will deal 1d6+1 psychic damage every turn its type is undead. This damage cannot be resisted or negated in any way.

***At higher levels:*** If you cast this spell using a spell slot of 2nd level or higher, the damage is increased by 1d4 and the duration is increased by 1 turn for every spell slot level above 1st.

##### Regret
*Enchantment*  
You make the sin currently weighing most on a creature's mind feel much heavier. The creature will feel the need to either seek to confession and/or atonement. If the sin is too heavy for the creature to bear, it might kill itself or go insane. Naturally evil creatures will instead feel great joy when affected by this spell.

##### Animated wounds
*Necromancy*  
Targets creature, creature is sickened for 1d4 turns. After that, either (a) taking piercing or slashing damage returns 1d6 (melee) or 1d4 (ranged) piercing damage, or (b) armor becomes 10 + number of times piercing or slashing damage was dealt (up to 18).
