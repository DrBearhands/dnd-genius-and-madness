\select@language {english}
\contentsline {part}{I\hspace {1em}New Races}{3}
\contentsline {chapter}{Undead servant}{4}
\contentsline {subsection}{Undead servant names}{4}
\contentsline {subsection}{Undead traits}{4}
\contentsline {subsubsection}{Skeleton}{4}
\contentsline {subsubsection}{Zombie}{4}
\contentsline {subsubsection}{Abomination}{4}
\contentsline {subsubsection}{Embalmed ones}{4}
\contentsline {part}{II\hspace {1em}New Classes}{5}
\contentsline {chapter}{Apothecary}{6}
\contentsline {chapter}{Artificer}{7}
\contentsline {subsection}{Arcane crafting}{7}
\contentsline {subsection}{Obsessive inventors}{7}
\contentsline {subsection}{Creating an artificer}{7}
\contentsline {subsubsection}{Quick build}{7}
\contentsline {section}{Class Features}{7}
\contentsline {subsubsection}{Hit Points}{7}
\contentsline {subsubsection}{Proficiencies}{8}
\contentsline {subsubsection}{Equipment}{8}
\contentsline {subsection}{Spellcasting}{8}
\contentsline {subsubsection}{Cantrips}{8}
\contentsline {subsubsection}{Known spells}{8}
\contentsline {subsubsection}{Spell slots}{8}
\contentsline {subsubsection}{Spellcasting ability}{8}
\contentsline {subsubsection}{Ritual casting}{9}
\contentsline {subsubsection}{Arcano-mechanical focus}{9}
\contentsline {subsection}{Specialization}{9}
\contentsline {subsection}{Invention}{9}
\contentsline {subsection}{Ability Score Improvement}{9}
\contentsline {subsection}{Extra attack}{9}
\contentsline {section}{Specializations}{9}
\contentsline {subsection}{Alchemy}{9}
\contentsline {subsubsection}{Battle Alchemy}{9}
\contentsline {subsubsection}{Potions}{9}
\contentsline {subsubsection}{Rapid oxidation}{10}
\contentsline {subsubsection}{Spellsoak potion}{10}
\contentsline {subsubsection}{LVL 14 feature}{10}
\contentsline {subsection}{Rune smitthing}{10}
\contentsline {subsubsection}{Quick runes}{10}
\contentsline {subsection}{Planar Channeling}{10}
\contentsline {subsubsection}{Channeling}{10}
\contentsline {subsubsection}{Favorite enemy}{10}
\contentsline {subsubsection}{Unleash}{11}
\contentsline {subsubsection}{Outsider exposure}{11}
\contentsline {subsubsection}{Banishment pariah}{11}
\contentsline {section}{Inventions}{11}
\contentsline {section}{Greater Inventions}{12}
\contentsline {chapter}{Oracle}{13}
\contentsline {subsection}{The passage of time}{13}
\contentsline {subsection}{Haunted by visions}{13}
\contentsline {subsection}{Creating an oracle}{13}
\contentsline {subsubsection}{Quick build}{13}
\contentsline {section}{Class Features}{13}
\contentsline {subsubsection}{Hit Points}{13}
\contentsline {subsubsection}{Proficiencies}{13}
\contentsline {subsubsection}{Equipment}{13}
\contentsline {subsection}{Visions}{13}
\contentsline {subsection}{Omen}{15}
\contentsline {subsubsection}{Omen spellcasting self-damage}{15}
\contentsline {subsubsection}{Omen Spellcasting ability}{15}
\contentsline {subsection}{Prophecy}{15}
\contentsline {subsection}{Manifest visions}{15}
\contentsline {subsection}{Predict misfortune}{15}
\contentsline {subsection}{Misfortune improvement}{16}
\contentsline {subsection}{Double omen}{16}
\contentsline {subsection}{Unravel the thread}{16}
\contentsline {section}{Prophecy}{16}
\contentsline {subsection}{The Divine Prophecy}{16}
\contentsline {subsubsection}{Standing}{16}
\contentsline {subsubsection}{Extended vision list}{16}
\contentsline {subsubsection}{Expanded spell list}{17}
\contentsline {subsection}{Prophecy of the void}{17}
\contentsline {subsubsection}{Expanded spell list}{17}
\contentsline {subsubsection}{Expanded visions list}{17}
\contentsline {subsection}{Prophecy of Glory}{17}
\contentsline {subsubsection}{Martial insight}{17}
\contentsline {subsubsection}{Expanded spell list}{17}
\contentsline {subsubsection}{Expanded visions list}{17}
\contentsline {chapter}{Psycher}{18}
\contentsline {part}{III\hspace {1em}New Subclasses}{19}
\contentsline {chapter}{Druid}{20}
\contentsline {section}{Druid circles}{20}
\contentsline {subsection}{Circle of roots}{20}
\contentsline {subsubsection}{Plants over beasts}{20}
\contentsline {subsubsection}{Expanded spell list}{20}
\contentsline {subsubsection}{Floral features}{20}
\contentsline {subsubsection}{Herbal mind}{20}
\contentsline {subsubsection}{Pronounced plant features}{20}
\contentsline {subsubsection}{Verdant growth}{20}
\contentsline {subsection}{Circle of cobbles}{20}
\contentsline {subsubsection}{Domestic shape}{21}
\contentsline {subsubsection}{Unnatural tooth and claw}{21}
\contentsline {subsubsection}{Unseen presence}{21}
\contentsline {subsubsection}{Expanded spell list}{21}
\contentsline {subsubsection}{Hive-mind}{21}
\contentsline {subsubsection}{Natural enough}{21}
\contentsline {subsubsection}{Toxic/corrosive}{21}
\contentsline {chapter}{Fighter}{22}
\contentsline {subsection}{Quartermaster}{22}
\contentsline {subsubsection}{Exotic arsenal}{22}
\contentsline {subsubsection}{Weapon maintenance}{22}
\contentsline {subsubsection}{Perfectly fitted}{22}
\contentsline {subsubsection}{Exquisite maintenance}{22}
\contentsline {subsubsection}{Renown}{22}
\contentsline {chapter}{Wizard}{23}
\contentsline {subsection}{Researcher}{23}
\contentsline {subsubsection}{Savant}{23}
\contentsline {subsubsection}{Published academic}{23}
\contentsline {subsubsection}{Improve spells}{23}
\contentsline {subsubsection}{Reverse engineer spells}{23}
\contentsline {subsubsection}{New spells}{23}
\contentsline {subsubsection}{Expanded spell list}{23}
\contentsline {part}{IV\hspace {1em}New Equipment}{24}
\contentsline {section}{Exotic weapons}{25}
\contentsline {subsection}{Special weapons}{25}
\contentsline {subsection}{Guns}{25}
\contentsline {subsubsection}{gun modifiers}{25}
\contentsline {section}{Exotic armor}{25}
\contentsline {part}{V\hspace {1em}Feats}{28}
\contentsline {subsection}{Gun expert}{29}
\contentsline {subsection}{Equipment specialist}{29}
\contentsline {subsection}{Flail Master}{29}
\contentsline {part}{VI\hspace {1em}Spells}{30}
\contentsline {section}{Secrets}{31}
\contentsline {section}{Crafting spells}{31}
\contentsline {section}{Spell lists}{32}
\contentsline {subsection}{Artificer spell list}{32}
\contentsline {subsection}{Oracle spell list}{32}
\contentsline {subsubsection}{Oracle Secrets}{33}
\contentsline {subsection}{Extended spell list: Bard}{33}
\contentsline {subsection}{Extended spell list: Druid}{33}
\contentsline {subsection}{Extended spell list: Ranger}{33}
\contentsline {subsection}{Extended spell list: Warlock}{33}
\contentsline {subsection}{Extended spell list: Wizard}{33}
\contentsline {subsubsection}{Wizard secrets}{33}
\contentsline {section}{Spell descriptions}{33}
\contentsline {subsubsection}{Assemble Siege engine}{33}
\contentsline {subsubsection}{Deadly needles}{34}
\contentsline {subsubsection}{Equivalent exchange}{34}
\contentsline {subsubsection}{Misfortune: death}{34}
\contentsline {subsubsection}{Misfortune: disaster}{34}
\contentsline {subsubsection}{Misfortune: disbelief}{34}
\contentsline {subsubsection}{Misfortune: harm}{34}
\contentsline {subsubsection}{Misfortune: ruin}{34}
\contentsline {subsubsection}{Misfortune: tragedy}{35}
\contentsline {subsubsection}{Misfortune: war}{35}
\contentsline {subsubsection}{Prepare bombs}{35}
\contentsline {chapter}{Resources and acknowledgement}{36}
\contentsline {section}{Used resources}{36}
\contentsline {section}{Inspiration}{36}
